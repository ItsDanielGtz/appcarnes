﻿using Android.App;
using Android.OS;
using Android.Runtime;
using AndroidX.AppCompat.App;
using Android.Widget;
using Plugin.Media;
using System;
using System.IO;
using Plugin.CurrentActivity;
using Android.Graphics;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;

namespace AppRegistroCarnes
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        string Archivo;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_main);
            SupportActionBar.Hide();
            CrossCurrentActivity.Current.Init(this, savedInstanceState);
            SetContentView(Resource.Layout.activity_main);
            SupportActionBar.Hide();
            CrossCurrentActivity.Current.Init(this, savedInstanceState);
            var Imagen = FindViewById<ImageView>(Resource.Id.Image);
            var btnAlmacenar = FindViewById<Button>(Resource.Id.btnGuardar);
            var txtNombre = FindViewById<EditText>(Resource.Id.txtCorte);
            var txtProveedor = FindViewById<EditText>(Resource.Id.txtProveedor);
            var txtFecha = FindViewById<EditText>(Resource.Id.txtCaducidad);
            var txtPrecio = FindViewById<EditText>(Resource.Id.txtPrecio);
            var txtExistencia = FindViewById<EditText>(Resource.Id.txtExistencia);
            Imagen.Click += async delegate
            {
                await CrossMedia.Current.Initialize();
                var archivo = await CrossMedia.Current.TakePhotoAsync
                (new Plugin.Media.Abstractions.StoreCameraMediaOptions
                {
                    Directory = "cortes",
                    Name = txtNombre.Text,
                    SaveToAlbum = true,
                    CompressionQuality = 30,
                    CustomPhotoSize = 30,
                    PhotoSize = Plugin.Media.Abstractions.PhotoSize.Medium,
                    DefaultCamera = Plugin.Media.Abstractions.CameraDevice.Front
                });
                if (archivo == null)
                {
                    return;
                }
                Bitmap bp = BitmapFactory.DecodeStream(archivo.GetStream());
                Archivo = System.IO.Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), txtNombre.Text + ".jpg");
                var stream = new FileStream(Archivo, FileMode.Create);
                bp.Compress(Bitmap.CompressFormat.Jpeg, 30, stream);
                stream.Close();
                Imagen.SetImageBitmap(bp);
            };
            btnAlmacenar.Click += async delegate
            {
                try
                {
                    var CuentaDeAlmacenamiento = CloudStorageAccount.Parse
                    ("");
                    var clienteblob = CuentaDeAlmacenamiento.CreateCloudBlobClient();
                    var carpeta = clienteblob.GetContainerReference("cortes");
                    var resourceblob = carpeta.GetBlockBlobReference(txtNombre.Text + ".jpg");
                    resourceblob.Properties.ContentType = "image/jpeg";

                    await resourceblob.UploadFromFileAsync(Archivo.ToString());
                    Toast.MakeText(this, "Imagen almacenada en el contendor de azure",
                    ToastLength.Long).Show();

                    var TablaNoSQL = CuentaDeAlmacenamiento.CreateCloudTableClient();
                    var Collercion = TablaNoSQL.GetTableReference("Carnes");
                    await Collercion.CreateIfNotExistsAsync();
                    var carne = new Carnes("Carnes", txtNombre.Text);
                    carne.Nombre_Provedor = txtProveedor.Text;
                    carne.Fecha_Cad = txtFecha.Text;
                    carne.Precio = txtPrecio.Text;
                    carne.No_Existencias = txtExistencia.Text;
                    carne.Imagen = "" + txtNombre.Text + ".jpg";
                    var store = TableOperation.Insert(carne);
                    await Collercion.ExecuteAsync(store);
                    Toast.MakeText(this, "Datos almacenados",
                    ToastLength.Long).Show();
                }
                catch (Exception ex)
                {
                    Toast.MakeText(this, ex.Message, ToastLength.Long).Show();
                }

            };
        }
        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
    public class Carnes : TableEntity
    {
        public Carnes(string Categoria, string Nombre)
        {
            PartitionKey = Categoria;
            RowKey = Nombre;
        }
        //campos iguales a la tabla
        public string Nombre_Provedor { get; set; }
        public string Fecha_Cad { get; set; }
        public string Precio { get; set; }
        public string No_Existencias { get; set; }
        public string Imagen { get; set; }
    }
}